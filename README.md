# **Formation : Découverte de python pour les enseignants du lycée** 

Sur cette page, vous retrouverez des liens vers les ressources présentées durant la formation.  
Cette présentation a été conçue dans un but de clarté et d'efficacité et ne se veut pas exhaustive.

# Thonny
Télécharger Thonny : [https://thonny.org/](https://thonny.org/)  

### Liste des raccourcis clavier :  
* ![F5](./doc/F5.jpg)  pour exécuter tout ce qui se trouve dans l'éditeur
* ![TAB](./doc/TAB.png)  assistant de saisie
* ![HAUT](./doc/HAUT.jpg)  dans la console pour saisir les instructions précédentes

### [Installation d’un nouveau module :](./doc/thonny.pdf)  
Cliquer sur **Tools**, puis **Manage packages**.  
Une fenêtre s’ouvre.  
Taper le nom du module à installer, faire **Search**, puis **Install**.  


# Maîtrise de Python
Voici des [accès à des notebooks](https://framagit.org/smalicet/formation/-/tree/master/python) sur les notions de bases :
1. Les variables
2. Les tests
3. Les listes
4. Les boucles
5. Les fonctions
6. Les modules
7. Les graphiques

[Memento python](./doc/MementoSeb.pdf)


# Le markdown
Ce langage va nous être utile pour écrire les notebooks.  
[Voici les éléments principaux de syntaxe](https://docs.framasoft.org/fr/grav/markdown.html#couleurs).  

On peut y insérer des formules mathématiques écrites en latex (mises entre $ $ ).  
Pour écrire facilement en latex :  [une formule](https://www.codecogs.com/latex/eqneditor.php) ou [un symbole](http://detexify.kirelabs.org/classify.html).  

# Jupyter notebook
Exemple introductif

# Quelques ressources :
En espérant que ces documents puissent vous servir de base à la préparation de TD : 
* [Physique-Chimie](https://framagit.org/smalicet/formation/-/tree/master/Ressources%20pour%20la%20PC)
* [Mathématiques](https://framagit.org/smalicet/formation/-/tree/master/Ressources%20pour%20les%20Maths)