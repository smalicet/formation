# 17 juin 2022
# Formation SNT Algo/programmation


## Logiciels et Applications :  
* IDE : [Installer Thonny](installation_thonny.pdf) , [installer_module](thonny.pdf)  
raccourcis :  
![F5](../doc/F5.jpg)  pour exécuter tout ce qui se trouve dans l'éditeur  
![TAB](../doc/TAB.png)  assistant de saisie  
![HAUT](../doc/HAUT.jpg)  dans la console pour saisir les instructions précédentes  

* Jupyter notebooks
* [Basthon](https://basthon.fr/) : pour lire ou faire des notebooks en ligne sans logiciel
* [Capytale](https://capytale2.ac-paris.fr/web/c-auth/list) : :tada: :tada: **SPOIL** :tada: :tada:  **: dispo rentrée 2022** : très grande base de données pour toutes les activités en python
* [Pythontutor](https://pythontutor.com/) : pour analyser l'exécution d'un programme
* [Algopython](https://www.algopython.fr/) : compte activé pour entrainement jusqu'au 1 septembre 2022
* [Tech.io](https://tech.io/playgrounds/56931/les-bases-de-python-pour-le-lycee/presentation) : Parcours lycée fait par Sébastien MALICET
* [Parcours algorea](https://parcours.algorea.org/contents/4703/) : créer un compte et rejoindre le groupe Enseignants_SNT

## Ressources :
* Algorithmique débranché : [Martin Quinson](https://github.com/InfoSansOrdi/CSIRL)    
3 activités d'introduction : le crépier, le jeu de nim et le baseball  
[livret](algo-livret.pdf), [petit livre](algo-petitlivre.pdf) et [documents à imprimer](algo-supports-vierge.pdf)
* Alien-python : [Mathieu Degrande](http://revue.sesamath.net/spip.php?article1510) :  
[fiche exercices 1 à 20](Exercices_Alien_Python.pdf) et [document à remplir](alien.pdf)
* Python-lycee.com : [Franck Chevrier](https://www.python-lycee.com/activite-en-ligne-snt)  
[TD sur les réseaux sociaux et graphes](https://notebook.basthon.fr/?from=https://raw.githubusercontent.com/PythonLycee/PyLyc/master/SNT_Graphes_Reseaux_Sociaux_vd.ipynb)


## Memento Python
* [Memento Python](../doc/MementoSeb.pdf)

## Questionnaire fin de séance :
[Enquête Dragnsurvey](https://form.dragnsurvey.com/survey/r/6d8c693e)
